var Firebase = require('firebase');
var _ = require('lodash');
var moment = require('moment');
var http = require('http');
var https = require('https');
var FCM = require('fcm-node');

var objectsRef = Firebase.database().ref().child("geysers");
var objects = [];

var convertBase64 = function(str) {
  if (Buffer.byteLength(str) !== str.length) throw new Error('bad string!');
  return Buffer(str, 'base64').readInt32BE();
}
var GetValueAsBool = function(val, size){
    var result = false;
    if (val && val.length > 0){
        result = (new Buffer(val, 'base64')[0] !== 0);
    }
    return result;
}

var GetValueAsInt = function(val, size){
    var result = 0;
    if (val && val.length > 0){
        result = new Buffer(val, 'base64').readInt32BE()
    }
    return result;
}

var GetValueAsString = function(val, size){
    var result = "";
    if (val && val.length > 0){
        result = new Buffer(val, 'base64').toString();
    }
    return result;
}


objectsRef.on('value', function(snapshot) {
    objects=[];
    for(value in snapshot.val()) {
        objects.push(snapshot.val()[value]);
    };
}); 



module.exports = {
    getDevice: function(device_id, query){
        return new Promise((resolve, reject) => {
            if(query.start && query.end){
                Firebase.database().ref().child("cars").child(device_id).orderByChild("date").startAt(query.start).endAt(query.end).once('value').then(function(snap) {
                    resolve(snap.val());
                });
            }else{
                Firebase.database().ref().child("cars").child(device_id).once('value').then(function(snap) {
                    resolve(snap.val());
                });
            }
        });
    },
    postData: function(req) {
        return new Promise((resolve, reject) => {
            
            
            var updateAsset = function(device_id, item){
                var fobj = Firebase.database().ref().child("cars").child(device_id).once('value').then(function(snap) {
                    var payload = item.payload;
                    console.log("1");
                    var logs;
                    var obj = snap.val();
                    if(!obj) obj = {};
                    
                    var updates = {};
                    for(var a in payload.fields){
                        var b64val = payload.fields[a].b64_value;
                        //var field_val = convertBase64(b64val);

                        

                        console.log("---");
                        var field_val = "";
                        if(a == "GPRMC_VALID"){
                            field_val = GetValueAsString(b64val, 1);
                        }else if(a == "GPS_SPEED"){
                            field_val = GetValueAsInt(b64val, 3)*1.852/1000; //convert to km/h (from knots)
                        }else if(a == "GPS_DIR"){
                            field_val = GetValueAsInt(b64val, 3);
                        }else if(a == "DIO_IGNITION"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "BATT"){
                            field_val = GetValueAsInt(b64val, 1);
                        }else if(a == "ODO_PARTIAL_KM"){
                            field_val = GetValueAsString(b64val, 4);
                        }else if(a == "ODO_FULL"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "GPS_PDOP"){
                            field_val = GetValueAsInt(b64val, 1);
                        }else if(a == "AREA_LIST"){
                            field_val = GetValueAsString(b64val, 32);
                        }else if(a == "GPS_FIXED_SAT_NUM"){
                            field_val = GetValueAsInt(b64val, 1);
                        }else if(a == "BEHAVE_ID"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "BEHAVE_LONG"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_LAT"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_DAY_OF_YEAR"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_TIME_OF_DAY"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_GPS_SPEED_BEGIN"){
                            field_val = GetValueAsInt(b64val, 8); //convert to km/h (from knots); 
                        }else if(a == "BEHAVE_GPS_SPEED_PEAK"){
                            field_val = GetValueAsInt(b64val, 8) //convert to km/h (from knots)
                        }else if(a == "BEHAVE_GPS_SPEED_END"){
                            field_val = GetValueAsInt(b64val, 8) //convert to km/h (from knots)
                        }else if(a == "BEHAVE_GPS_HEADING_BEGIN"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_GPS_HEADING_PEAK"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_GPS_HEADING_END"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_X_BEGIN"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_X_PEAK"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_X_END"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Y_BEGIN"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Y_PEAK"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Y_END"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Z_BEGIN"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Z_PEAK"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ACC_Z_END"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_ELAPSED"){
                            field_val = GetValueAsInt(b64val, 8);
                        }else if(a == "BEHAVE_UNIQUE_ID"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_CRASH_DETECTED"){
                            field_val = GetValueAsString(b64val, 8);
                        }else if(a == "EVENT"){
                            field_val = GetValueAsString(b64val, 128);
                        }else if(a == "MDI_EXT_BATT_LOW"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_EXT_BATT_VOLTAGE"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_PANIC_STATE"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_PANIC_MESSAGE"){
                            field_val = GetValueAsString(b64val, 50);
                        }else if(a == "MDI_DTC_MIL"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_DTC_NUMBER"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_DTC_LIST"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_RPM_MAX"){
                            field_val = GetValueAsInt(b64val, 5);
                        }else if(a == "MDI_RPM_MIN"){
                            field_val = GetValueAsInt(b64val, 5);
                        }else if(a == "MDI_RPM_AVERAGE"){
                            field_val = GetValueAsInt(b64val, 5);
                        }else if(a == "MDI_RPM_OVER"){
                            field_val = GetValueAsBool(b64val, 4);
                        }else if(a == "MDI_RPM_AVERAGE_RANGE_1"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_RPM_AVERAGE_RANGE_2"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_RPM_AVERAGE_RANGE_3"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_RPM_AVERAGE_RANGE_4"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_SENSORS_RECORDER_DATA"){
                            field_val = GetValueAsString(b64val, 32);
                        }else if(a == "MDI_SENSORS_RECORDER_CALIBRATION"){
                            field_val = GetValueAsString(b64val, 32);
                        }else if(a == "ODO_PARTIAL_METER"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "ODO_FULL_METER"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OBD_PID_1"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_OBD_PID_2"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_OBD_PID_3"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_OBD_PID_4"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_OBD_PID_5"){
                            field_val = GetValueAsString(b64val, 255);
                        }else if(a == "MDI_DASHBOARD_MILEAGE"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_DASHBOARD_FUEL"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_DASHBOARD_FUEL_LEVEL"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_DIAG_1"){
                            field_val = GetValueAsString(b64val, 128);
                        }else if(a == "MDI_DIAG_2"){
                            field_val = GetValueAsString(b64val, 128);
                        }else if(a == "MDI_DIAG_3"){
                            field_val = GetValueAsString(b64val, 128);
                        }else if(a == "MDI_PENDING_DTC_LIST"){
                            field_val = GetValueAsString(b64val, 128);
                        }else if(a == "MDI_MAX_RPM_IN_LAST_OVER_RPM"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OBD_MILEAGE_METERS"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_IDLE_STATE"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_VEHICLE_STATE"){
                            field_val = GetValueAsString(b64val, 3);
                        }else if(a == "MDI_OBD_SPEED"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OBD_RPM"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OBD_FUEL"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OBD_VIN"){
                            field_val = GetValueAsString(b64val, 17);
                        }else if(a == "MDI_OBD_MILEAGE"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_JOURNEY_TIME"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_IDLE_JOURNEY"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_DRIVING_JOURNEY"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_MAX_SPEED_IN_LAST_OVERSPEED"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OVERSPEED_COUNTER"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_TOW_AWAY"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_ODO_JOURNEY"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_OVERSPEED"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_MAX_SPEED_JOURNEY"){
                            field_val = GetValueAsInt(b64val, 4);
                        }else if(a == "MDI_JOURNEY_STATE"){
                            field_val = GetValueAsBool(b64val, 1);
                        }else if(a == "MDI_RECORD_REASON (deprecated)"){
                            field_val = GetValueAsString(b64val, 10);
                        }else if(a == "ENH_DASHBOARD_MILEAGE"){
                            field_val = GetValueAsString(b64val, 32);
                        }else if(a == "ENH_DASHBOARD_FUEL"){
                            field_val = GetValueAsString(b64val, 32);
                        }else if(a == "ENH_DASHBOARD_FUEL_LEVEL"){
                            field_val = GetValueAsString(b64val, 32);
                        }
                        payload.fields[a].val = field_val;
                    }
                    
                    if(payload && payload.recorded_at) payload.date = new Date(payload.recorded_at).getTime();
                    for(var a in payload.fields){
                        var field = payload.fields[a];
                        //if(a == )

                    }
                    if(item.meta.event == "track"){
                        //logs = (obj.track) ? obj.track : [];
                        //logs.push(payload);
                        //obj.track = logs;
                        var newPostKey = Firebase.database().ref().child('cars/' + device_id + "/track").push().key;
                        updates["/cars/" + device_id + "/track/" + newPostKey] = payload;
                    }else if(item.meta.event == "presence"){
                        //logs = (obj.presence) ? obj.presence : [];
                        //logs.push(payload);
                        //obj.presence = logs;
                        var newPostKey = Firebase.database().ref().child('cars/' + device_id + "/presence").push().key;
                        console.log("adding presence");
                        updates["/cars/" + device_id + "/presence/" + newPostKey] = payload;
                    }else if(item.meta.event == "message"){
                        //logs = (obj.message) ? obj.message : [];
                        //logs.push(payload);
                        //obj.message = logs;
                        var newPostKey = Firebase.database().ref().child('cars/' + device_id + "/messages").push().key;
                        updates["/cars/" + device_id + "/messages/" + newPostKey] = payload;
                    }
                    Firebase.database().ref().update(updates);
                    //fobj.update(obj);
                    
                });
            }
            var requests = JSON.parse(req.body);
            
            for(var i=0;i<requests.length;i++){
                //console.log("requests", i);
                var item = requests[i];
                var device_id = item.payload.asset;
                var payload = item.payload;
                console.log("oki");
                updateAsset(device_id, item);
            }
            resolve(req.body);
        });
    }
};
 