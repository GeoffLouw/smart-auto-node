var basicAuth = require('basic-auth');
module.exports = {
    // Basic auth function
    auth: function (req, res, next) {

        var user = basicAuth(req);

        if (!user || !user.name || !user.pass) {
            return unauthorized(res);
        } else if (user.name === 'foo' && user.pass === 'bar') {
            return next();
        } else {
            return unauthorized(res);
        };
    },

    authAdmin: function (req, res, next) {
        var user = basicAuth(req);
        if (!user || !user.name || !user.pass) {
            return unauthorized(res);
        } else if (user.name === 'admin' && user.pass === 'admin') {

            return next();
        } else {
            return unauthorized(res);
        };
    }

};

function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.sendStatus(401);
};