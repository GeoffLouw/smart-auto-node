var https = require('https');
var fs = require('fs');
var express = require('express');
var app = express();
var Firebase = require('firebase');
var bodyParser = require('body-parser');


//app.use( bodyParser.raw({type: '*/*'}));       // to support JSON-encoded bodies
      // to support JSON-encoded bodies
app.use( bodyParser.text({type: '*/*'})); 


// Firebase initialization
/* LIVE
Firebase.initializeApp({ 
    apiKey: 'AIzaSyATNlJ8L9jRmjFdz-rtONFxEOzGPKK2orU',
    authDomain: 'sensor-smart-tracker.firebaseapp.com',
    databaseURL: 'https://sensor-smart-tracker.firebaseio.com',
    storageBucket: 'sensor-smart-tracker.appspot.com',
});
 */
/*DEV*/
Firebase.initializeApp({ 
    apiKey: "AIzaSyBbs5zlwkNIp1P4OjRHj7E21xzsQUY323E",
    authDomain: "sensor-smart-tracker-dev.firebaseapp.com",
    databaseURL: "https://sensor-smart-tracker-dev.firebaseio.com",
    storageBucket: "sensor-smart-tracker-dev.appspot.com",
    messagingSenderId: "990866494678"
});
global.serverKey = 'AIzaSyAPWrk028BG7xJPeELJVn8aI6NwfbIJokA';
global.isdev = true;
global.isdebug = true;

var restapi = require('./js/restapi');
var auth = require('./js/auth');

/*-------REST API-----------*/
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});
app.get('/ping', function (req, res) {
    res.send('Ping System Date: ' + new Date().toString());
});
app.post('/ping', function (req, res) {
    res.send('Ping System Date: ' + new Date().toString());
});
app.get('/report/', function (req, res) {
    restapi.getData(req).then(
        result => res.send(result),
        error => res.sendStatus(500)
    );
});
app.post('/report/', function (req, res) {
    restapi.postData(req).then(
        result => res.send(result),
        error => res.sendStatus(500)
    );
});
app.get('/devices', function (req, res) {
    restapi.getDevices().then(
        data => res.send(data),
        error => res.sendStatus(500)
    );
});
app.get('/devices/:id', function(req, res) {
    restapi.getDevice(req.params.id, req.query).then(
        data => res.send(data),
        error => res.sendStatus(500)
    );
});
app.get('/', function (req, res) {
    restapi.postData(req).then(
        result => res.send("success"),
        error => res.sendStatus(500)
    );
});

var port = 3005;

/*-------HTTPS SERVER START------------*/
/*var options = {
  key: fs.readFileSync('ssl/key.pem'),
  cert: fs.readFileSync('ssl/cert.pem')
};

https.createServer(options, app).listen(port);
console.log('Server listening on port '+port);*/

/*-------HTTP SERVER START------------*/
app.listen(port, function() {
    console.log('Server listening on port '+port);
})
